public class Individuo implements Notificable{

    private String nombre;
    private String DNI;
    private String direccion;
    private String telefono;

    //CONSTRUCTOR
    public Individuo(String nombre, String DNI, String direccion, String telefono) {
        this.nombre = nombre;
        this.DNI = DNI;
        this.direccion = direccion;
        this.telefono = telefono;
        this.direccion = "pepi";
    }

    //EQUALS PARA COMPARAR OBJETOS EMPRESA
    @Override
    public boolean equals(Object obj) {
        Individuo otroIndividuo = (Individuo) obj;
        return (this.DNI.equals(otroIndividuo.DNI));
    }

    @Override
    public void notificar(Diario diarioRecibido) {
        diarioRecibido.getNombreDiario();
    }
}
