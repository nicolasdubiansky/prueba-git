public class Empresa implements Notificable {

    private String nombreDeFantasia;
    private String CUIT;
    private Integer cantidadDeEmpleados;
    private String telefono;

    //CONSTRUCTOR


    public Empresa(String nombreDeFantasia, String CUIT, Integer cantidadDeEmpleados, String telefono) {
        this.nombreDeFantasia = nombreDeFantasia;
        this.CUIT = CUIT;
        this.cantidadDeEmpleados = cantidadDeEmpleados;
        this.telefono = telefono;
    }

    //EQUALS PARA COMPARAR OBJETOS EMPRESA
    @Override
    public boolean equals(Object obj) {
        Empresa otroIndividuo = (Empresa) obj;
        return (this.CUIT.equals(otroIndividuo.CUIT));
    }

    @Override
    public void notificar(Diario diarioRecibido) {
        diarioRecibido.getNombreDiario();
    }
}
