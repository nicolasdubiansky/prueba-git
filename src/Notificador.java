import java.util.ArrayList;
import java.util.List;

public abstract class Notificador {

    protected List<Notificable> notificables = new ArrayList<>();

    //LE AGREGO LA PALABRA ABSTRACT PARA NO ESCRIBIR ACA EL CODIGO DEL METODO
    public abstract void notificar();


    public void agregarNotificable(Notificable notificable){
        notificables.add(notificable);
    }

    public void eliminarNotificable(Notificable notificable){
        notificables.remove(notificable);
    }
}
