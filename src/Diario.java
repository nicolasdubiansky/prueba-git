public class Diario extends Notificador{

    private String nombreDiario;

    public Diario(String nombreDiario) {
        this.nombreDiario = nombreDiario;
    }

    @Override
    public void notificar() {
        for (Notificable unNotificable: notificables) {
            unNotificable.notificar(this);
        }
    }

    public String getNombreDiario() {
        return nombreDiario;
    }
}
